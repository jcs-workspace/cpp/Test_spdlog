@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-04-11 14:08:25 $
:: $Revision: $
:: $Creator:  $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

set VCPKG_ROOT=D:\Program Files\vcpkg\

cd ../

echo ::Build MSVC solution
cmake -S . -B build/windows/
